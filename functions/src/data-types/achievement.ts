export type Achievement = {
  description: string,
  type: string,
  reward: number,
  requirement: 1,
  title: string
}