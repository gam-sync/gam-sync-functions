import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const OneSignal = require('onesignal-node');

admin.initializeApp();

let client = new OneSignal.Client({      
  userAuthKey: 'ZTg1NmNmOTQtNWEzMS00NmY5LTkyOWMtN2Y0NDUzZDYxMDI0',      
  app: { appAuthKey: 'MjJiOTAxZGYtY2I0Yi00ZWQ0LTg5YmMtMTA3ODg4ZDQ3ZDdj', appId: '8b45cfd5-ebee-46ef-b00d-47008edb83d6' }      
}); 




exports.createEvent = functions.region('europe-west1').firestore.document('events/{event}').onCreate((snap, context) => {
    const firestore = admin.firestore();
    return firestore.collection('users').where('isAdmin', '==', true).get().then((admins) => {
      const adminId: Array<any> = [];
      admins.docs.forEach((element) => {
        const userData = element.data();
        adminId.push(userData['oneSignalId']);
      });

      const notification = new OneSignal.Notification({
          contents: {
            es: 'Un usuario ha subido un tablón. Revisalo.',
            en: 'Un usuario ha subido un tablón. Revisalo.'
          },
          include_player_ids: adminId
        });

      client.sendNotification(notification, (error: any, res: any, data1: any) => {
        if (error) {
          console.log()
        } else {
          console.log(data1);
        }
      });

    });
})

exports.achievementFriend = functions.region('europe-west1').firestore.document('users/{userId}/friends/{friendId}').onWrite((change, context) => {
    const firestore = admin.firestore();
    const afterData = change.after.data();


    if (afterData !== undefined) {
        if (afterData.status === 1) {
            firestore.collection('achievements').where('type', '==', 'friend').get().then((achievements) => {
              const friendAchievements: Array<any> = [];
              achievements.docs.forEach(element => friendAchievements.push(element.data()));
              firestore.collection('users').doc(context.params.userId).collection('friends').get().then((user) => {
                const friendCount = user.docs.length;
                // tslint:disable-next-line: prefer-for-of
                for (let i = 0; i < friendAchievements.length; i++) {
                  if(friendAchievements[i].requirement <= friendCount) {
                    firestore.collection('users').doc(context.params.userId).collection('achievements').doc(friendAchievements[i].achievement_id).get()
                    .then((achievement) => {
                      if (!achievement.exists) achievement.ref.set({
                        title: friendAchievements[i].title,
                        description: friendAchievements[i].description,
                        asset: friendAchievements[i].asset
                      })
                    });
                  }
                }
              });
            });
            
        }
    }

    return true;
    
});

exports.achievementGames = functions.region('europe-west1').firestore.document('users/{userId}/games/{gameId}').onWrite((change, context) => {
  const firestore = admin.firestore();
  const afterData = change.after.data();

  console.log(afterData);


  if (afterData !== undefined) {
          firestore.collection('achievements').where('type', '==', 'game').get().then((achievements) => {
            const gamesAchievements: Array<any> = [];
            achievements.docs.forEach(element => gamesAchievements.push(element.data()));
            firestore.collection('users').doc(context.params.userId).collection('games').get().then((user) => {
              const gamesCount = user.docs.length; 
              // tslint:disable-next-line: prefer-for-of
              for (let i = 0; i < gamesAchievements.length; i++) {
                if(gamesAchievements[i].requirement <= gamesCount) {
                  firestore.collection('users').doc(context.params.userId).get()
                  .then((userAchievement) => {
                    // Assign achievement
                    userAchievement.ref.collection('achievements').doc(gamesAchievements[i].achievement_id).get().then((achievementUser) => {
                      if (!achievementUser.exists) {
                        achievementUser.ref.set({
                          title: gamesAchievements[i].title,
                          description: gamesAchievements[i].description,
                          asset: gamesAchievements[i].asset
                        })
                      }
                      
                    });

                    const userData = userAchievement.data();
                    if (userData !== undefined) {
                      userAchievement.ref.update({
                        xp: userData['xp'] === null || userData['xp'] === undefined ? gamesAchievements[i].reward : userData['xp'] + gamesAchievements[i].reward
                      });
                    }
                  });
                }
              }
            });
          });
          
  }

  return true;
  
});


exports.gainXpChat = functions.region('europe-west1').firestore.document('rooms/{roomId}/messages/{messageId}').onWrite((change, context) => {
  const firestore = admin.firestore();
  const afterData = change.after.data();

  console.log(afterData);


  if (afterData !== undefined) {
     firestore.collection('users').doc(afterData.userId).get().then((user) => {
        const userData = user.data();
        if (userData !== undefined) {
          const xpRandom =  Math.floor(Math.random() * (6 - 1) + 1) * 2;
          user.ref.update({
            xp: userData['xp'] === null || userData['xp'] === undefined ? xpRandom : userData['xp'] + xpRandom
          })
        }
        
        
     });
  }

  return true;
  
});